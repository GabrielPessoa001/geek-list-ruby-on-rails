Rails.application.routes.draw do
  resources :animes
  resources :series
  resources :movies
  resources :books
  resources :games
  devise_for :users
  
  root "pages#index"
end
